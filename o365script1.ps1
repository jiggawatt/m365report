﻿
Import-Module ExchangeOnlineManagement
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 

# CONNECT MSONLINE
Connect-MsolService

# ADD ALL CLIENTS WITHOUT MAILBOX TO EXCLUDED DOMAINS
$excludedDomains = @()

# GET LIST OF ALL PARTNER TENANTID EXCLUDING EXCLUDED ONES
$customers = Get-MsolPartnerContract -All | Where-Object {$_.DefaultDomainName -notin $excludedDomains}
Write-Host "Found $($customers.Count) customers for $((Get-MsolCompanyInformation).displayname)." -ForegroundColor DarkGreen

# PATH TO OUTPUT CSV
$CSVpath = "C:\b.csv"

# ITERATE THROUGH EACH TENANT
foreach ($customer in $customers) {
    Write-Host "Retrieving info for $($customer.name)" -ForegroundColor Green
    # GET ALL LICENSED USERS IN TENANT
    $licensedUsers = Get-MsolUser -TenantId $customer.TenantId -All | Where-Object {$_.islicensed}
    $emailDomain = $customer.DefaultDomainName
    # CONNECT TO EXCHANGE ONLINE FOR TENANT
    Connect-ExchangeOnline -DelegatedOrganization $emailDomain
    
    # ITERATE THROUGH USERS IN TENANT
    foreach ($user in $licensedUsers) {
        Write-Host "$($user.displayname)" -ForegroundColor Yellow  
        $licenses = $user.Licenses
        $licenseArray = $licenses | foreach-Object {$_.AccountSkuId}
        $licenseString = $licenseArray -join ", "
        $lastLogin = Get-EXOMailboxStatistics -Identity $user.UserPrincipalName -Properties LastUserActionTime | Select LastUserActionTime
        $isMFAenabled = if ($user.StrongAuthenticationMethods) {$true} else {$false}
        $licensedSharedMailboxProperties = [pscustomobject][ordered]@{
            CustomerName      = $customer.Name
            DisplayName       = $user.DisplayName
            Licenses          = $licenseString
            #TenantId          = $customer.TenantId
            UserPrincipalName = $user.UserPrincipalName
            MFAEnabled        = $isMFAenabled
            LastEmailLogin    = $lastLogin.LastUserActionTime
        }
        # APPEND USER DATA TO CSV
        $licensedSharedMailboxProperties | Export-CSV -Path $CSVpath -Append -NoTypeInformation   
        
    }
# CLOSE ALL PS SESSION AFTER EACH ITERATION THRU TENANT
Get-PSSession | Remove-PSSession
}




